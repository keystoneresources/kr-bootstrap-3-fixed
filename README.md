# H5BP + Bootstrap
### Current Versions:
H5BP 4.2.0
Bootstrap 2.3.1

### phpThumbOf & LessPhp Nginx Rewrites
	# rewrites for passing width and height to phpThumb via URL
	location @phpthumbWH {
	  rewrite ^/i/(\d+)/(\d+)/(.+)$ /pt/phpThumb.php?w=$1&h=$2&phptfn=$1/$2/$3&src=/$3 last;
	}

	# rewrites for passing just width to phpThumb via URL
	location @phpthumbW {
	  rewrite ^/i/(\d+)/(.+)$ /pt/phpThumb.php?w=$1&phptfn=$1/$2&src=/$2 last;
	}

	# handle requests to width-and-height thumbs
	location ~ ^/i/(\d+)/(\d+)/(cf/)?(.*)\.(jpe?g|png)$ {
	  expires 7d;
	  try_files /pt/cache/$1/$2/$4.$5 @phpthumbWH;
	}

	# handle requests to width-only thumbs
	location ~ ^/i/(\d+)/(cf/)?(.*)\.(jpe?g|png)$ {
	  expires 7d;
	  try_files /pt/cache/$1/$3.$4 @phpthumbW;
	}

	location / {
	    try_files $uri $uri/ @modx-rewrite;
	    #lessphp compiler
	    rewrite ^/([^.]*\.less)$ /compilers/lessphp.php?file=$1 redirect;
	}